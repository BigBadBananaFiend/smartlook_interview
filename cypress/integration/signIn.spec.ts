/// <reference types="cypress" />

// This file will contain tests for the Sign In form. Feel free to add as many as you want.
// You dont have to test successful sign in as an account is needed for it. Other scenarios are recommended to test.  

context('Sign in', () => {
  beforeEach(() => {
    // 
    cy.visit('https://app.alfa.smartlook.cloud/sign/in')

    // Get elements + assign aliases
    cy.get('#sign-in-form--email-input--inner').as('e-mail') // e-mail input field
    cy.get('#sign-in-form--password-input--inner').as('password') // password input field
    cy.get('#sign-in-form--submit').as('submit') // submit button
  })


  // Generate random password/login method

  function random(dataType: string, len: number): string {
    let result: string = '';
    let chars: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charsLen: number = chars.length;

    for (let i: number = 0; i < len; i++) {
      result += chars.charAt(Math.floor(Math.random() * charsLen))
    };

    if (dataType == 'E-mail') {
      result += '@testmail.npm'
    }
    return result;
  }


  // Test Cases

  it('TC1: Submit form with only e-mail field filled in', () => {
    let missingPass: string = 'Please enter your password';
    cy.get('@e-mail')
      .type(random('E-mail', 5))
    cy.get('@submit')
      .click()
    cy.get('[data-cy=password-input-error]')
      .should('contain', missingPass)
  })

  it('TC2: Submit form with only password field filled in', () => {
    let missingEmail: string = 'Please fill in your email address';
    cy.get('@e-mail').clear()
    cy.get('@password')
      .type(random('pass', 7))
    cy.get('@submit').click()
    cy.get('[data-cy=email-input-error]')
      .should('contain', missingEmail)
  })

  it('TC3: Login with invalid email/password', () => {
    cy.get('@e-mail')
      .type(random('E-mail', 5))
    cy.get('@password')
      .type(random('pass', 6))
    cy.get('@submit')
      .click()
    cy.get('.dev-vw9qt2.e1lp8wu80')
      .should('contain', 'Something went wrong')

  })

  it('TC4: Sumbit the form by pressing Enter', () => {
    cy.get('@e-mail')
      .type(random('E-mail', 5))
    cy.get('@password')
      .type(random('pass', 6))
      .type('{enter}')
    cy.get('.dev-vw9qt2.e1lp8wu80')
      .should('contain', 'Something went wrong')
  })

  it('TC5: Check the password placeholder', () => {
    cy.get('@password')
      .should('have.attr', 'placeholder')
      .and('eq', 'At least 6 characters')
  })

  it('TC6: Check a password of < 6 chars', () => {
    let shortPass: string = 'Your password must be at least 6 characters long'
    cy.get('@password')
      .type(random('pass', 4))
    cy.get('.dev-1klsz87.engguxp1', {
      timeout: 30000
    }).should('contain', shortPass);
  })

  it('TC7: Check if the password set to non-visible by default', () => {
    cy.get('@password')
      .type(random('pass', 3));
    cy.get('.dev-3oqqxl.e1iiq0x40')
      .as('pass-visibility')
      .should('have.attr', 'name')
      .and('eq', 'show-eye')
    cy.screenshot('hidden-password');
  })

  it('TC8: Make password visible and take a screenshot', () => {
    cy.get('#sign-in-form--password-input--show-password-btn')
      .click()
    cy.get('.dev-3oqqxl.e1iiq0x40')
      .should('have.attr', 'name')
      .and('eq', 'show-eye-off')
    cy.get('@password')
      .type(random('pass', 3));
    cy.screenshot('visible-password');
  })

  it('TC9: Check the e-mail placeholder', () => {
    cy.get('@e-mail')
      .should('have.attr', 'placeholder')
      .and('eq', 'my@email.com')
  })

  it('TC10: Check if e-mail in invalid format is accepted', () => {
    let invalidEmail: string = 'That doesn\'t look like a valid email address'
    cy.get('@e-mail')
      .type(random('test', 10))
    cy.get('.dev-1klsz87.engguxp1').should('contain', invalidEmail)
  })

  it('TC11: Forgotten password: Invalid E-mail', () => {
    let invalidEmail: string = 'That doesn\'t look like a valid email address'
    cy.get('#sign-in-form--reset-password-link')
      .click();
    cy.url()
      .should('eq', 'https://app.alfa.smartlook.cloud/sign/reset-password');
    cy.get('#reset-password-form--email-input--inner')
      .type(random('test', 5));
    cy.get('.dev-1klsz87.engguxp1')
      .should('contain', invalidEmail)


  })

  // Feel free to modify this file and other files to you liking except the url.

})