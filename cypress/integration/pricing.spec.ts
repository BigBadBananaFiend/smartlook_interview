/// <reference types="cypress" />


// This file will contain tests for the Pricing page. Feel free to add as many as you want.
//https://www.alfa.smartlook.cloud/pricing/?currencyCode=CZK

context('Pricing', () => {
  beforeEach(() => {
    cy.visit('https://www.alfa.smartlook.cloud/pricing/?currencyCode=CZK')
  })

  // Formatting function for price string based on selected Session Limit and Data History values within the business plan modal
  function formatItem(dataHistory: string, price: string, session: number, planType:string): string {
    let result: string = 'CZK ';
    let incrementsAnnual: number[] = [360, 520, 880, 1240, 1600, 1920]
    let incrementsMonthly: number[] = [450, 650, 1100, 1550, 2000, 2400]
    let baseIncrement:number;
    planType == 'annual' ? baseIncrement = incrementsAnnual[session] : baseIncrement = incrementsMonthly[session]
    let temp: number;
    let index: number = price.indexOf(',');
    temp = Number(price.replace(',', ''));

    if (dataHistory == '1 month') {
      return price
    } else if (dataHistory == '3 months') {
      temp += baseIncrement;
      if (temp >= 10000 && temp < 100000) {
        index = 2
      }
      result += [String(temp).slice(0, index), ',', String(temp).slice(index)].join('')
    } else if (dataHistory == '6 months') {
      temp += baseIncrement * 2;
      if (temp >= 10000 && temp < 100000) {
        index = 2
      }
      result += [String(temp).slice(0, index), ',', String(temp).slice(index)].join('')
    } else if (dataHistory == '12 months') {
      temp += baseIncrement * 3;
      if (temp >= 10000 && temp < 100000) {
        index = 2
      }
      result += [String(temp).slice(0, index), ',', String(temp).slice(index)].join('')
    }
    return result;
  }

  // Function to check the prices relevent to each modal 
  function checkPrices(col: number, heading: string, price: string): void {
    cy.get('.package')
      .eq(col)
      .within(() => {
        cy.get('.package-name__heading')
          .should('contain', heading)
        cy.get('.package-price__heading')
          .contains(price)
      })
  }

  // Startup Modal Testing Method 
  function startUpModal(subPlan: string[], session: string[]) {
    // Check the modal  
    for (let i: number = 0; i < session.length; i++) {
      if (session[i] != 'more') {
        cy.get('@limits').select(session[i])
        cy.get('.pricing-modal__package')
          .within(() => {
            cy.get('@price').contains(subPlan[i])
          })
      } else {
        cy.get('@limits').select(session[i])
        cy.get('.modal-price.modal-price--loading').should('have.css', 'filter', 'blur(5px)')
      }
    }
  }

  // Business Modal Testing Method
  function businessModalTest(subPlan: string[], session: string[], dataHistory: string[], planType:string): void {
    for (let i: number = 0; i < session.length; i++) {
      cy.get('@limits').select(session[i])
      for (let j: number = 0; j < dataHistory.length; j++)
        if (session[i] != 'more') {
          cy.get('@data')
            .select(dataHistory[j]);
          cy.log(formatItem(dataHistory[j], subPlan[i], i, planType))
          cy.get('@price').contains(formatItem(dataHistory[j], subPlan[i], i, planType))
        }
      else {
        cy.get('@limits').select(session[i])
        cy.get('.modal-price.modal-price--loading').should('have.css', 'filter', 'blur(5px)')
      }
    }
  }

  // Startup Data 
  let startUpSubPlan: string[][] = [
    ['CZK 879', 'CZK 1,039', 'CZK 1,599', 'CZK 2,239'],
    ['CZK 1,099', 'CZK 1,299', 'CZK 1,999', 'CZK 2,799']
  ]; // [annual] [monthly]
  let startUpSessionLimit: string[] = ['5,000', '7,500', '15,000', '25,000', 'more'];
  let startUpChoosePlan: string[] = ['annual', 'monthly'];

  // Business data
  let businessSubPlan: string[][] = [
    ['2,079', '2,639', '3,519', '5,279', '7,039', '8,799'],
    ['2,599', '3,299', '4,399', '6,599', '8,799', '10,999']
  ]; // Level 1: Annual prices, Level 2: monthly prices
  let businessSessionLimit: string[] = ['15,000', '25,000', '50,000', '100,000', '150,000', '200,000', 'more'];
  let dataHistory: string[] = ['1 month', '3 months', '6 months', '12 months'];

  // Test Cases
  it('1: Check Packages', () => {

    let columns: string[] = ['Free', 'Startup', 'Business', 'Ultimate'];
    let prices: string[] = ['Free', 'CZK 879 / monthly', 'CZK 2,079 / monthly', 'Contact sales'];

    for (let i: number = 0; i < columns.length - 1; i++) {
      checkPrices(i, columns[i], prices[i]);
    }
  })


  it('2: Startup: Annual Modal', () => {

    // Open Startup "Build Plan" button
    cy.get('.package')
      .eq(1)
      .within(() => {
        cy.get('#startup-package-button')
          .click()
      })

    // Get aliases for fields/buttons
    cy.get('.modal-limits__item', {timeout: 4000})
      .contains('Sessions limit')
      .within(() => {
        cy.get('.input')
          .as('limits')
      })
    cy.get('.modal-price')
      .as('price')


    // Run the check modal function //annual
    startUpModal(startUpSubPlan[0], startUpSessionLimit)

  })

  it('3: Startup: Monthly Modal', () => {

    // Open Startup "Build Plan" button
    cy.get('.package')
      .eq(1)
      .within(() => {
        cy.get('#startup-package-button')
          .click()
      })

    // Get aliases for fields/buttons
    cy.get('.modal-limits__item')
      .contains('Sessions limit')
      .within(() => {
        cy.get('.input')
          .as('limits')
      })

    // Change sub plan to monthly
    cy.get('.modal-price')
      .as('price')
    cy.get('.radio')
      .eq(0)
      .click()

    // Run the check modal function //monthly
    startUpModal(startUpSubPlan[1], startUpSessionLimit)

  })

  it('4. Business: Annual Modal', () => {

    // Open Business "Build Plan" button
    cy.get('.package')
      .eq(2)
      .within(() => {
        cy.get('#business-package-button')
          .click()
      })

    // Get aliases for fields/buttons
    cy.get('.modal-limits__item')
      .contains('Sessions limit')
      .within(() => {
        cy.get('.input')
          .as('limits')
      })
    cy.get('.modal-price')
      .as('price')
    cy.get('.modal-limits__item', {timeout: 4000})
      .contains('Data history')
      .within(() => {
        cy.get('.input')
          .as('data')
      })

    // Run the check modal function //annual 
    businessModalTest(businessSubPlan[0], businessSessionLimit, dataHistory, 'annual')

  })

  it('5. Business: Monthly Modal', () => {

    // Open Business "Build Plan" button
    cy.get('.package')
      .eq(2)
      .within(() => {
        cy.get('#business-package-button')
          .click()
      })

    // Get aliases for fields/buttons
    cy.get('.modal-limits__item', {timeout: 4000})
      .contains('Sessions limit')
      .within(() => {
        cy.get('.input')
          .as('limits')
      })
    cy.get('.modal-price')
      .as('price')
    cy.get('.modal-limits__item', {timeout: 4000})
      .contains('Data history')
      .within(() => {
        cy.get('.input')
          .as('data')
      })

    // Change sub plan to monthly
    cy.get('.modal-price')
      .as('price')
    cy.get('.radio')
      .eq(0)
      .click()

    // Run the check modal function //monthly
    // Run the check modal function //annual 
    businessModalTest(businessSubPlan[1], businessSessionLimit, dataHistory, 'monthly')

  }) 

})