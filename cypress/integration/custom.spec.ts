/// <reference types="cypress" />

// You can test something else in here (be creative :)
context('New account creation', () => {
    beforeEach(() => {
      cy.visit('https://app.alfa.smartlook.cloud/sign/up/')

      // Get elements + assign aliases
      cy.get('#sign-up-form--email-input--inner')
        .as('email')
      cy.get('#sign-up-form--password-input--inner')
        .as('password')
      cy.get('#sign-up-form--consent-part-1')
        .as('confirmation')
      cy.get('#sign-up-form--submit')
        .as('submit')
    })

    // Generate random password/login method

  function random(dataType: string, len: number): string {
    let result: string = '';
    let chars: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charsLen: number = chars.length;

    for (let i: number = 0; i < len; i++) {
      result += chars.charAt(Math.floor(Math.random() * charsLen))
    };

    dataType == 'E-mail' ? result += '@fakemail.npm' : result += 'PA$$'

    return result;
  }

    it('TC:1 –> Sign up with correct e-mail and no password', () => {
        cy.get('@email')
          .type(random('E-mail', 5))
        cy.get('@confirmation')
          .click()
        cy.get('@submit')
          .click()
        cy.get('[data-cy="password-input-error"]')
          .should('contain', 'Please enter your password')
        
    })

    it('TC:2 –> Sign up with correct e-mail and invalid password', () => {
        cy.get('@email')
          .type(random('E-mail', 1))
        cy.get('@password')
          .type(random('pass', 1))
        cy.get('@confirmation')
          .click()
        cy.get('[data-cy=password-input-error]', {timeout: 4000})
          .should('contain', 'Your password must be at least 8 characters long')        
    })

    it('TC:3 –> Sign up no e-mail and correct password', () => {
        cy.get('@password')
          .type(random('pass', 5))
        cy.get('@confirmation')
          .click()
        cy.get('@submit')
          .click()
        cy.get('[data-cy=email-input-error]')
          .should('contain', 'Please fill in your email address')            
    })

    it('TC4: Sign up with invalid e-mail and correct password', () => {
        cy.get('@email')
          .type('noDomainMail')
        cy.get('@password')
          .type(random('pass', 5))
        cy.get('@confirmation')
          .click()
        cy.get('.dev-6mf4zi--disabled.efvt08p0')
        cy.get('[data-cy=email-input-error]', {timeout: 4000})
          .should('contain', 'That doesn\'t look like a valid email address')  
        
    })

    it('TC:5: Sign up with correct e-mail + password –> T&C not checked', () => {
        cy.get('@email')
          .type(random('E-mail', 5))
        cy.get('@password')
          .type(random('pass', 5))
        cy.get('@submit')
          .click()
        cy.get('#sign-up-form--consent-part-1')
          .should('have.css', 'color', 'rgb(255, 91, 108)')

    })



})